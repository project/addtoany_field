<?php

namespace Drupal\addtoany_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'addtoany_field' formatter.
 *
 * @FieldFormatter(
 *   id = "addtoany_field",
 *   label = @Translation("AddToAny Field Formatter"),
 *   field_types = {
 *     "addtoany_field"
 *   }
 * )
 */
class AddToAnyFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {

    $element = [];

    foreach ($items as $delta => $item) {

      // Initialize the return build array.
      $build = [];

      if (method_exists($item, 'getEntity')) {

        $entity = $item->getEntity();

        if ($entity instanceof ContentEntityInterface) {

          // AddToAny Data - depends on addtoany module.
          $data = addtoany_create_entity_data($entity);

          // Convert URL to absolute.
          // Check for option to "self link", if no use url.
          if (empty($this->getFieldSettings()['self_link'])) {
            $url = Url::fromUri($item->getValue()['uri'], ['absolute' => TRUE])->toString();
            $title = $item->getValue()['title'] ?? $data['link_title'];
          }
          // If self_link is yes, use the path to the node as the share link.
          else {
            $alias = \Drupal::service('path_alias.manager')->getAliasByPath('/node/' . $entity->id());
            $url = Url::fromUserInput($alias, ['absolute' => TRUE])->toString();
            $title = $entity->label();
          }

          // Same as addtoany code, but replace URL use link title is available.
          $build = [
            '#addtoany_html'              => $data['addtoany_html'],
            '#link_url'                   => $url,
            '#link_title'                 => $title,
            '#button_setting'             => $data['button_setting'],
            '#button_image'               => $data['button_image'],
            '#universal_button_placement' => $data['universal_button_placement'],
            '#buttons_size'               => $data['buttons_size'],
            '#theme'                      => 'addtoany_standard',
            '#cache'                      => [
              'contexts' => ['url'],
            ],
            '#attached' => [
              'library' => 'addtoany/addtoany',
            ],
          ];

          $build['#addtoany_html'] = \Drupal::token()->replace($data['addtoany_html'], ['node' => $entity]);

        }
      }

      // Render each element as markup.
      $element[$delta] = $build;

    }

    return $element;

  }

}
