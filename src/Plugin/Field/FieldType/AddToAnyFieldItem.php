<?php

namespace Drupal\addtoany_field\Plugin\Field\FieldType;

use Drupal\Core\Form\FormStateInterface;
use Drupal\link\Plugin\Field\FieldType\LinkItem;

/**
 * Plugin implementation of the 'addtoany_field' field type.
 *
 * @FieldType(
 *   id = "addtoany_field",
 *   label = @Translation("AddToAny"),
 *   description = @Translation("Shares the specified link using AddToAny."),
 *   default_widget = "addtoany_field",
 *   default_formatter = "addtoany_field",
 *   cardinality = 1,
 *   constraints = {"LinkType" = {}, "LinkAccess" = {},
 *    "LinkExternalProtocols" = {}, "LinkNotExistingInternal" = {}}
 * )
 */
class AddToAnyFieldItem extends LinkItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'self_link' => 0,
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::fieldSettingsForm($form, $form_state);

    $elements['self_link'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Always Link to Self'),
      '#default_value' => $this->getSetting('self_link'),
      '#description' => $this->t('The link should always point to this node.  This is useful when embedding this into a view or a block.  Enabling this option will hide the field from the form, and will always use the value to the node alias.<br />This setting should <strong>NOT</strong> be used if you are using multiple values for this field.<br />This option sets default values on this field, and is not visible on the edit form.'),
    ];

    return $elements;
  }

}
