<?php

namespace Drupal\addtoany_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\link\Plugin\Field\FieldWidget\LinkWidget;

/**
 * Plugin implementation of the 'addtoany_field' widget.
 *
 * @FieldWidget(
 *   id = "addtoany_field",
 *   label = @Translation("AddToAny"),
 *   field_types = {
 *     "addtoany_field"
 *   }
 * )
 */
class AddToAnyFieldWidget extends LinkWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'self_link' => 0,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    // User defines the share link.
    if (!$this->getFieldSettings()['self_link']) {
      return parent::formElement($items, $delta, $element, $form, $form_state);
    }
    // Share link always points to self - use placeholders.
    else {
      $element += [
        'label' => [
          '#type' => 'markup',
          '#markup' => '<h4>' . $element['#title'] . ' (hidden / default value)</h4>',
        ],
        'uri' => [
          '#type' => 'hidden',
          '#value' => '<nolink>',
        ],
        'title' => [
          '#type' => 'hidden',
          '#value' => '<title>',
        ],
      ];
    }
    return $element;
  }

}
