# AddToAny Field

## Field type extended from core link called "AddToAny" to specify a link and title to share through AddToAny.

CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration


INTRODUCTION
------------

Creates a field type for "addtoany_field", based on the link field. Useful if you are trying to share a link through AddToAny that is different from the page you are on.

Share any link by entering the link in the field of type "AddToAny" (addtoany_field). Alternatively, if you are always wanting to share the existing node that the field is on, you can use the "self_link" switch on the field settings. This is useful if you are displaying the node in a view or a block.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/addtoany_field

* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/addtoany_field


REQUIREMENTS
------------

This module requires the following modules:

* [Drupal Core Link](https://www.drupal.org/project/drupal)
* [AddToAny](https://www.drupal.org/project/addtoany)


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

### Default
* Add a field of type AddToAny (under General).
* The field widget is the same as a link field.
* Update the share "URL" and "Link text" fields.
* Use the "Manage Display" settings to place the field.
* The formatter will automatically be set to the "AddToAny Field Formatter".

### Self Link
When editing the field settings, there is an option for "Always Link to Self".  When enabled, the link should always
point to this node. This is useful when embedding this into a view or a block. Enabling this option will hide the field
from the form, and will always use the value to the node alias.
